<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('tpl/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Task modification</h1>
					<?php
					$query = $db -> prepare('SELECT * FROM task WHERE id = ?');
	        $query -> execute(array($_GET['id']));
	        $data = $query -> fetch();
					?>
					<form method="post" action="update.php" class="small-12 medium-6 collumn">
						<input name="id" type="hidden" value="<?php echo $_GET['id']; ?>"/>
            <label>Description</label>
            <textarea name="description" rows="6"><?php echo $data['description']; ?></textarea>
            <label>Priority</label>
            <select name="priority">
							<option value="<?php echo $data['priority']; ?>"><?php echo $data['priority']; ?></option>
              <?php for($i = 1; $i <= 5; $i++): ?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
              <?php endfor; ?>
            </select>
            <label>Delay</label>
            <input type="date" name="due_at" value="<?php echo $data['due_at']; ?>"/>
						<label>Assigned to</label>
						<select name="assigned_to">
							<?php
							$query = $db -> query('SELECT * FROM user');
							while($data =	$query -> fetch()):
							?>
								<option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
							<?php
							endwhile;
							?>
            </select>
            <input type="submit" value="Edit" class="button"/>
	        </form>
				</div>
			</main>
		</div>
  </body>
</html>
