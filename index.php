<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('tpl/head.php'); ?>
  </head>
  <body>
		<?php require_once('tpl/header.php'); ?>
			<ul class="tasklist">
				<li class="tasklist-item row hide-for-small-only ">
          <span class="tasklist-item-id large-1 columns">N°</span>
          <span class="tasklist-item-description large-4 columns">Description</span>
          <span class="tasklist-item-created_by large-2 columns">Created by</span>
          <span class="tasklist-item-due_at large-2 columns">Due at</span>
          <span class="tasklist-item-assigned_to large-2 columns">To do by</span>
          <span class="tasklist-item-priority large-1">Priority</span>
        </li>

				<?php
	        $query = $db -> prepare('SELECT
																		task.id,
																		description,
																		created_at,
																		due_at,
																		priority,
																		status,
																		creator.id as creator_id,
																		creator.name as creator_name,
																		assignee.id as assignee_id,
																		assignee.name as assignee_name
																		FROM task
																		INNER JOIN user as creator on created_by = creator.id
																		LEFT JOIN user as finishor on done_by = finishor.id
																		INNER JOIN user as assignee on assigned_to = assignee.id');
					$query -> execute();
	        while($data = $query -> fetch()):
	        ?>

	        <li class="tasklist-line row <?php if($data['status'] == 'close'):?>
                     tasklist-line-done row<?php endif;?>">
						<div class="tasklist-item-id small-12 large-1 columns">
							<?php echo $data['id']; ?>
						</div>
          	<div class="tasklist-item-description large-4 columns">
          		<?php echo $data['description']; ?>
          	</div>
          	<div class="tasklist-item-created_by small-12 large-2 columns">
              <span class="hide-for-medium">Created by: </span>
          		<?php echo $data['creator_name']; ?>
          	</div>
          	<div class="tasklist-item-due_at small-12 large-2 columns">
              <span class="hide-for-medium">Due at: </span>
          		<?php echo $data['due_at']; ?>
          	</div>
          	<div class="tasklist-item-assigned_to small-12 large-2 columns">
              <span class="hide-for-medium">Assigned to: </span>
          		<?php echo $data['assignee_name']; ?>
          	</div>
          	<div class="tasklist-item-priority large-1 columns">
              <span class="hide-for-medium">Priority: </span>
          		<?php echo $data['priority']; ?>
          	</div>
          	<div class="tasklist-item-appli">
          		<a href="#" data-done="<?php echo $data['id']; ?>" class="done">
	              <i class="fa fa-check" aria-hidden="true"></i>
	            </a>
           		<a href="edit.php?id=<?php echo $data['id']; ?>" class="change">
	              <i class="fa fa-pencil" aria-hidden="true"></i>
	            </a>
	            <a href="#" data-delete="<?php echo $data['id']; ?>" class="delete">
	              <i class="fa fa-times" aria-hidden="true"></i>
	            </a>
          	</div>
        	</li>
          <?php endwhile;?>
      	</ul>
        <a class="add" href="add.php">
      		<i class="fa fa-plus-circle" aria-hidden="true"></i>
      	</a>
			<?php require('tpl/footer.php'); ?>
  </body>
</html>
