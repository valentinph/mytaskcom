<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
    <?php require_once('tpl/head.php'); ?>
  </head>
  <body>
    <div class="off-canvas-wrapper">
      <?php require_once('tpl/header.php'); ?>

      <main class="container off-canvas-content" data-off-canvas-content>
        <div class="row">
          <h1 class="page-title">Valentin Pharisa</h1>
          <img src="assets/img/profil_1.jpg" style="width: 15%; height: 15%;">
          <h2>Infos personnelles</h2>
          <ul>
            <li>Adresse: Cité Mont-Repos 8</li>
            <li>1635 La Tour-de-Trême</li>
            <li>23 ans</li>
          </ul>
          <h2>Formation</h2>
          <ul>
            <li>2016 - : Bachelor en télécommunication, HEIA-FR</li>
            <li>2010 - 2014: Apprentissage d'électronicien, HEIA-FR</li>
          </ul>
          <h2>Expérience professionnelle</h2>
          <ul>
            <li>2016 - 2017 : Agent de circulation, Protect'service SA</li>
            <li>2015 - 2016: Militaire contractuelle, Armée Suisse</li>
          </ul>
        </div>
      </main>
    </div>
  </body>
</html>
