<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('tpl/head.php'); ?>
  </head>
		<div class="off-canvas-wrapper">
			<?php require_once('tpl/header.php');	?>
					<h1 class="page-title">User list</h1>
					<ul class="tasklist row">
						<li class="tasklist-item row hide-for-small-only">
							<span class="tasklist-item-id large-2 columns">ID</span>
          		<span class="tasklist-item-created_by large-2 columns">User</span>
          		<span class="tasklist-item-description large-6 columns end">Email</span>


						</li>
						<?php
	          $query = $db -> query('SELECT * FROM user');
	          while($data = $query -> fetch()):
	          ?>
						<li class="tasklist-line row">
	            <div class="tasklist-item-id small-12 large-2 columns">
	              <?php echo $data['id']; ?>
	            </div>
	            <div class="tasklist-item-created_by small-12 large-2 columns">
	              <?php echo $data['name']; ?>
	            </div>
							<div class="tasklist-item-description large-3 columns end ">
	              <?php echo $data['email']; ?>
	            </div>
	            <div class="tasklist-item-appli">
								<a href="edituser.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-pencil" aria-hidden="true"></i>
	              </a>
	              <a href="#" data-deleteuser="<?php echo $data['id']; ?>">
	                <i class="fa fa-times" aria-hidden="true"></i>
	              </a>
	            </div>
	          </li>
	          <?php endwhile; ?>
					</ul>
		</div>
</html>
