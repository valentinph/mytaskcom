$(document).foundation();

$('[data-done]').click(function(e) {
	var elt = $(this);

	$.ajax({
		url: 'done.php?id='+elt.data('done')
	})
	.done(function() {
		$(elt).parents('.tasklist-line').addClass('tasklist-line-done');
	})
});

$('[data-delete]').click(function(e) {
	var elt = $(this);

	$.ajax({
		url: 'delete.php?id='+elt.data('delete')
	})
	.done(function() {
		$(elt).parents('.tasklist-line').remove();
	})
});

$('[data-deleteuser]').click(function(e) {
	var elt = $(this);

	$.ajax({
		url: 'deleteuser.php?id='+elt.data('deleteuser')
	})
	.done(function() {
		$(elt).parents('.tasklist-item').remove();
	})
});
