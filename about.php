<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
    <?php require_once('tpl/head.php'); ?>
  </head>
  <body>
    <div class="off-canvas-wrapper">
      <?php require_once('tpl/header.php'); ?>

      <main class="container off-canvas-content" data-off-canvas-content>
        <div class="row">
          <h1 class="page-title">About</h1>
          <p>
            Le projet mytask.com est un gestionnaire de tâche intéractif. Il peut être utilisé par plusieurs utilisateurs en même temps et permet de classer les tâches selon différents critères (Priorité, status, créateur de la tâche...etc).
            <br><br>
            Contexte: <br>
            Il a été réalisé dans le cadre du cours de I-communication des classes de télécommunication, orientaton internet et communication de 1ère années
            <br><br>
            Date: <br>
            Année scolaire 2016-2017
            <br>
          </p>
        </div>
      </main>
    </div>
  </body>
</html>


