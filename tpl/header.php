<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
 <header class="row expanded">
  <div class="title-bar">
    <div class="title-bar-left">
      <button class= "menu-icon" type="button" data-open="offCanvasLeft"></button>
      <span class="title-bar-title"><a href="index.php">Task manager</a></span>
    </div>
    <div class="title-bar-right">

			<?php
			$query =	$db -> prepare('SELECT * FROM user WHERE id = ?');
			$query -> execute(array($_SESSION['userid']));
			$data = $query -> fetch();
			?>

      <ul class="dropdown menu" data-dropdown-menu>
        <li>
        	<a href="#"><img src="assets/img/profil_<?php echo $_SESSION['userid']; ?>.jpg">
        	</a>
          <ul class="menu">
            <li>
            	<a href="edituser.php?id=<?php echo $_SESSION['userid']; ?>"><?php echo $data['name']; ?></a>
            </li>
						<li>
							<a href="logout.php">Logout</a>
						</li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <nav class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
    <div class="column">
      <ul class="menu vertical">
        <li><a href="adduser.php">New user</a></li>
				<li><a href="users.php">User list</a></li>
    		<li><a href="../assets/doc/User_Manual.pdf">User Manual</a></li>
        <li><a href="../assets/doc/Technical_Manual.pdf">Technical Manual</a></li>
        <li><a href="cv.php">Personnal CV</a></li>
        <li><a href="about.php">About</a></li>
      </ul>
    </div>
  </nav>
</header>

